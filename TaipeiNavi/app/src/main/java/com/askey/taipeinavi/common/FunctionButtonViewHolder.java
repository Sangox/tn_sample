package com.askey.taipeinavi.common;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.askey.taipeinavi.R;

import tw.sango.ui.RecyclerViewHolderHandler;

/**
 * Created by Sango(sango.sangox@gmail.com) on 2017/1/4.
 */

class FunctionButtonViewHolder extends RecyclerViewHolderHandler {
    private ImageView mIcon;
    private TextView mName;

    public void bindHolder(View view) {
        mIcon = (ImageView)view.findViewById(R.id.imgIcon);
        mName = (TextView)view.findViewById(R.id.txtName);
    }

    @Override
    public void bindSubView(Context context, Object data) {
        FunctionButtonObject obj = (FunctionButtonObject)data;

        mIcon.setImageResource(obj.iconResId);
        mName.setText(obj.nameResId);
    }
}
