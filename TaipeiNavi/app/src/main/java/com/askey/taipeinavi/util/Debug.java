package com.askey.taipeinavi.util;

/*
 * Copyright (C) 2015-2016 Sango(sango.sangox@gmail.com) All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * V1.0
 */

import android.annotation.SuppressLint;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Sango(sango.sangox@gmail.com) on 2016/12/13.
 */

public class Debug {
    private static final String TAG = "TaipeiNavi";
    private static long startTime = 0;
    private static long endTime = 0;

    public static void errLog(String msg) {
        Log.e(TAG, msg);
    }

    public static void testLog(String msg) {
        Log.d(TAG, msg);
    }

    public static void infoLog(String msg) {
        Log.d(TAG, msg);
    }

    public static void logStartTime() {
        startTime = endTime = System.currentTimeMillis();
    }

    public static void logEndTime() {
        endTime = System.currentTimeMillis();
    }

    public static long getLogTimeDistance() {
        return getLogTimeDistance(null);
    }

    @SuppressLint("DefaultLocale")
    public static long getLogTimeDistance(String tag) {
        long distance = (endTime - startTime);

        if (tag != null) {
            testLog(String.format("## %s used time:%d", tag, distance));
        } else {
            testLog("## used time:" + distance);
        }
        return distance;
    }

    public static void writeLog() {
        try {
            Process process = Runtime.getRuntime().exec("logcat -d");
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            StringBuilder log=new StringBuilder();
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                log.append(line).append("\n");
            }

            FileOutputStream fos = null;
            DataOutputStream dos = null;

            try {
                fos = new FileOutputStream(new File(Environment.getExternalStorageDirectory(), "checkmeLog.txt"));
                dos = new DataOutputStream(fos);
                dos.writeUTF(log.toString());
            } catch (FileNotFoundException fnfe) {
                System.out.println("File not found" + fnfe);
            } catch (IOException ioe) {
                System.out.println("Error while writing to file" + ioe);
            } finally {
                try {
                    if (dos != null) {
                        dos.close();
                    }
                    if (fos != null) {
                        fos.close();
                    }
                } catch (Exception e) {
                    System.out.println("Error while closing streams" + e);
                }
            }
        } catch (IOException e) {
        }
    }
}
