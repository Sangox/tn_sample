package com.askey.taipeinavi;

import android.content.Intent;
import android.os.Bundle;

import com.askey.taipeinavi.home.HomeActivity;

import butterknife.ButterKnife;
import tw.sango.ui.DefaultFragmentActivity;

public class TaipeiNaviActivity extends DefaultFragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taipei_navi);
        ButterKnife.bind(this);

        super.setStatusBarColor(R.color.white);

        // TODO: Remove these after developing
        Intent intent = new Intent(this, HomeActivity.class);
        this.startActivity(intent);
        this.finish();
    }

    @Override
    public void onBackPressed() {
        // block back event
    }
}
