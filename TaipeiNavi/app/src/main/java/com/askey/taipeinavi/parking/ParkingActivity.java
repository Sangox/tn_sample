package com.askey.taipeinavi.parking;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.askey.taipeinavi.R;

/**
 * Created by Sango(sango.sangox@gmail.com) on 2017/6/26.
 */

public class ParkingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking);

        TextView txtTitle = (TextView)findViewById(R.id.txtToolbarTitle);
        txtTitle.setText("停車資訊");

        View backButton = findViewById(R.id.tbtnToolbarBack);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
