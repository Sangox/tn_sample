package com.askey.taipeinavi.home;

import android.os.Bundle;

import tw.sango.ui.BaseContainerFragment;
import tw.sango.ui.DefaultFragmentActivity;
import tw.sango.ui.OnBackPressedListener;

/**
 * Created by Sango(sango.sangox@gmail.com) on 2016/12/7.
 */

public class HomeBaseFragment extends BaseContainerFragment {
    private DefaultFragmentActivity mParentActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mParentActivity = (DefaultFragmentActivity)getActivity();
        mParentActivity.setBackPressedListener(new BackPressedListener());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (!super.mIsViewInited) {
            super.mIsViewInited = true;
            HomeFragment homeFrag = new HomeFragment();

            replaceFragment(homeFrag, false);
        }
    }

    private class BackPressedListener implements OnBackPressedListener {
        @Override
        public boolean onBack() {
            return popFragment();
        }
    }
}
