package com.askey.taipeinavi.common;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.askey.taipeinavi.home.HomeActivity;
import com.askey.taipeinavi.home.HomeBaseFragment;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Sango(sango.sangox@gmail.com) on 2017/6/16.
 */

public abstract class BaseChildFragment extends Fragment {
    protected HomeActivity mParentActivity;
    protected HomeBaseFragment mParentFragment;
    private Unbinder mUnbinder;

    protected abstract int getContentView();

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mParentActivity =  (HomeActivity)getActivity();
        mParentFragment = (HomeBaseFragment)getParentFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(getContentView(), container, false);
        mUnbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }
    }

    @Override
    public void onDestroy() {
        mParentFragment = null;
        mParentActivity = null;
        super.onDestroy();
    }
}
