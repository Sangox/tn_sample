package com.askey.taipeinavi.home;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.askey.taipeinavi.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import tw.sango.ui.DefaultFragmentActivity;

public class HomeActivity extends DefaultFragmentActivity {
    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.toolbarHome) View mToolbarHome;
    @BindView(R.id.toolbarNormal) View mToolbarNormal;
    @BindView(R.id.txtToolbarTitle) TextView mTxtToolbarTitle;
    @BindView(R.id.tbtnToolbarBack) View mTbtnToolbarBack;

    private HomeBaseFragment mHomeBaseFrag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);

        mHomeBaseFrag = new HomeBaseFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.flayContainer, mHomeBaseFrag).commit();

        setStatusBarColor(R.color.main_status_bar);
        setSupportActionBar(mToolbar);
        registerToolBar(mToolbar, R.id.txtToolbarTitle, -1, -1);
    }

    @OnClick(R.id.tbtnToolbarBack)
    public void onBackClicked() {
        onBackPressed();
    }

    public void showHomeToolbar() {
        mToolbarHome.setVisibility(View.VISIBLE);
        mToolbarNormal.setVisibility(View.GONE);
    }

    public void showNormalToolbar() {
        showNormalToolbar(-1);
    }

    public void showNormalToolbar(int titleResId) {
        mToolbarHome.setVisibility(View.GONE);
        mToolbarNormal.setVisibility(View.VISIBLE);

        if (titleResId != -1) {
            mTxtToolbarTitle.setText(titleResId);
        }
    }

    @Override
    public void onBackPressed() {
        if (super.mBackListener == null || !super.mBackListener.onBack()) {
            super.onBackPressed();
        }
    }
}
