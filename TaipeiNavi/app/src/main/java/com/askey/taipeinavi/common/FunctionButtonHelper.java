package com.askey.taipeinavi.common;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.askey.taipeinavi.R;

import java.util.ArrayList;

import tw.sango.ui.OnRecyclerViewItemClickListener;
import tw.sango.ui.RecyclerViewBaseAdapter;
import tw.sango.ui.RecyclerViewHelper;

/**
 * Created by Sango(sango.sangox@gmail.com) on 2017/6/12.
 */

public class FunctionButtonHelper {
    public static View getHomeFuncView(Context context, ViewGroup container, OnRecyclerViewItemClickListener listener) {
        FunctionButtonObject btnObj;
        ArrayList<FunctionButtonObject> funcList = new ArrayList<>();

        btnObj = new FunctionButtonObject();
        btnObj.iconResId = R.mipmap.icon_traininfo;
        btnObj.nameResId = R.string.btxt_parking;
        funcList.add(btnObj);

        return getFuncView(context, container, listener, funcList);
    }

    private static View getFuncView(Context context,ViewGroup container, OnRecyclerViewItemClickListener listener, ArrayList<FunctionButtonObject> funcList) {
        GridLayoutManager gm = new GridLayoutManager(context, 2);

        RecyclerViewBaseAdapter adapter = new RecyclerViewBaseAdapter(new FunctionButtonViewHolder(), funcList, R.layout.item_fuction_button);
        View funcView = RecyclerViewHelper.getView(context, container, gm, adapter, listener);

        RecyclerView rcView = (RecyclerView)funcView.findViewById(R.id.rcView);
        rcView.setNestedScrollingEnabled(false);
        rcView.addItemDecoration(new FunctionButtonItemDecoration(context));

        return funcView;
    }
}
