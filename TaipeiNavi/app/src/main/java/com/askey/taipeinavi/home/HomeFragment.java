package com.askey.taipeinavi.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.askey.taipeinavi.R;
import com.askey.taipeinavi.common.BaseChildFragment;
import com.askey.taipeinavi.common.FunctionButtonHelper;
import com.askey.taipeinavi.parking.ParkingActivity;

import butterknife.BindView;
import butterknife.OnClick;
import tw.sango.ui.OnRecyclerViewItemClickListener;

/**
 * Created by Sango(sango.sangox@gmail.com) on 2016/12/7.
 */

public class HomeFragment extends BaseChildFragment {
    @BindView(R.id.prLayMain) LinearLayout mLLayMain;

    @Override
    protected int getContentView() {
        return R.layout.fragment_home;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();
    }

    @Override
    public void onResume() {
        mParentActivity.showHomeToolbar();

        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void initView() {
        View funcView = FunctionButtonHelper.getHomeFuncView(getContext(), mLLayMain, new RecyclerViewItemClickListener());
        mLLayMain.addView(funcView);
    }

    private class RecyclerViewItemClickListener implements OnRecyclerViewItemClickListener {
        @Override
        public void onItemClicked(View view, int position) {
            switch(position) {
                case 0: // Parking
                    Intent intent = new Intent(getActivity(), ParkingActivity.class);
                    getActivity().startActivity(intent);
                    break;
            }
        }
    }
}
