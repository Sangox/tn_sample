package com.askey.taipeinavi.common;

import android.content.Context;

import tw.sango.ui.GridSpacingItemDecoration;
import tw.sango.util.Util;

/**
 * Created by Sango(sango.sangox@gmail.com) on 2017/6/13.
 */

class FunctionButtonItemDecoration extends GridSpacingItemDecoration {
    FunctionButtonItemDecoration(Context context) {
        super(2, (int) Util.dipToPixels(context, 10), false, 0);
    }
}
