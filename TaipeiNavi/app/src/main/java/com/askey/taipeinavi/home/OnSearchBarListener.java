package com.askey.taipeinavi.home;

/**
 * Created by Sango(sango.sangox@gmail.com) on 2017/6/18.
 */

public interface OnSearchBarListener {
    void onCancel();
    void onTyping(String text);
}
